+++
title = "Legendaddy"
outputs = ["Reveal"]
+++

# The Man, The Myth, The Legend

---

His fortune cookies simply read: "Congratulations!"

---

In a past life he was himself

---

His legend precedes him, the way lightening precedes thunder

---

He is ...

---

{{< slide background-image="/me.jpg" >}}
